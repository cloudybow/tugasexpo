import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Component from "./Tugas/Quiz3/index";
// "./Tugas/Tugas12/App"
// './Tugas/Tugas13/LoginScreen'
//"./Tugas/Tugas13/About"
//"./Tugas/Tugas14/App"
//"./Tugas/Tugas14/SkillScreen"
//"./Tugas/Tugas15/index"
//"./Tugas/TugasNavigation/index"

export default function App() {
  return (
    <Component />
    // <View style={styles.container}>
    //   <Text>Hello, Vee! Welcome to your first App Project! Keep Doing Your Best, You Can Do It!</Text>
    // <StatusBar style="auto" />
    // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});