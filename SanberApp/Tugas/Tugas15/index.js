import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {SignIn,CreateAccount,Profile,Home,Search,Details,Search2} from './Screen';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';

const AuthStack = createStackNavigator();
const Tab = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const SearchStack = createStackNavigator();
const ProfileStack = createStackNavigator();
const Drawer = createDrawerNavigator();

const HomeStackScreen = () => (
  <HomeStack.Navigator>
    <HomeStack.Screen name="Home" component={Home}/>
    <HomeStack.Screen name="Details" component={Details} options={({route})=>(
      {title:route.params.name}
    )}/>
  </HomeStack.Navigator>
)
const SearchStackScreen = () => (
  <SearchStack.Navigator>
    <SearchStack.Screen name="Search" component={Search}/>
    <SearchStack.Screen name="Search2" component={Search2}/>
  </SearchStack.Navigator>
)
const ProfileStackScreen = () => (
  <ProfileStack.Navigator>
    <ProfileStack.Screen name="Profile" component={Profile}/>
  </ProfileStack.Navigator>
)
const Tabs = ()=>(
  <Tab.Navigator>
    <Tab.Screen name="Home" component={HomeStackScreen}/>
    <Tab.Screen name="Search" component={SearchStackScreen}/>
  </Tab.Navigator>
)
export default () => (
    <NavigationContainer>
      <Drawer.Navigator>
        <Drawer.Screen name="Home" component={Tabs}/>
        <Drawer.Screen name="Profile" component={ProfileStackScreen}/>
      </Drawer.Navigator>
      {/* <AuthStack.Navigator>
        <AuthStack.Screen name='SignIn' component={SignIn} options={{title : "Sign In"}}/>
        <AuthStack.Screen name='CreateAccount' component={CreateAccount} options={{title : "Create Account"}}/>
      </AuthStack.Navigator> */}
    </NavigationContainer>
);