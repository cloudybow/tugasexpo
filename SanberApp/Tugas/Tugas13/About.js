import React, { Component } from 'react';
import {StyleSheet, 
        View, 
        Text,
        StatusBar} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class loginScreen extends Component {
    render(){
        return (
            <View style={styles.Container}>
            <StatusBar backgroundColor="blue"/>
                <View style={styles.headContainer}>
                    <Text style={{color:'#003366', fontSize:35}}>Tentang Saya</Text> 
                    <Icon name="account-circle" size={100} style={{marginTop:30,marginBottom:10}}/>
                    <View style={styles.nameHead}>
                        <Text style={{fontSize:23,color:'#003366'}}>Afiyah Salsabila Arief</Text>
                        <Text style={{color:'#3EC6FF'}}>Future React Native Developer</Text>
                    </View>
                </View>
                <View style={styles.portContainer}>
                    <Text style={{fontSize:18,borderBottomWidth:1,marginTop:5,marginLeft:15,color:'#003366',borderColor:'#003366'}}>Portofolio</Text>
                    <View style={styles.portLogoWrap}>
                        <View style={styles.portLogoContainer}>
                            <Icon name="gitlab" size={25} color='#3EC6FF'/>
                            <Text style={{color:'#003366'}}>@cloudybow</Text>
                        </View>
                        <View style={styles.portLogoContainer}>
                            <Icon name="google-drive" size={25} color='#3EC6FF'/>
                        <Text style={{color:'#003366'}}>@afiiyahsarief</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.hubContainer}>
                    <Text style={{fontSize:18,borderBottomWidth:1,marginTop:5,marginLeft:15,color:'#003366',borderColor:'#003366'}}>Hubungi Saya</Text>
                    <View style={styles.hubLogoWrap}>
                        <View style={styles.hubLogoContainer}>
                            <Icon name="facebook" size={25} color='#3EC6FF' style={{marginRight:5}}/>
                            <Text style={{color:'#003366'}}>Afiyah Salsabila Arief</Text>
                        </View>
                        <View style={styles.hubLogoContainer}>
                            <Icon name="instagram" size={25} color='#3EC6FF' style={{marginRight:5}}/>
                            <Text style={{color:'#003366'}}>@afiiyahsarief_</Text>
                        </View>
                        <View style={styles.hubLogoContainer}>
                            <Icon name="telegram" size={25} color='#3EC6FF' style={{marginRight:5}}/>
                            <Text style={{color:'#003366'}}>@afiiyahsarief</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    Container:{
        flex:1
    },
    headContainer:{
        alignItems:'center',
        marginTop:20
    },
    nameHead:{
        alignItems:'center'
    },
    portContainer:{
        marginVertical:30,
        backgroundColor:'#EFEFEF',
        borderRadius:15,
        width:329,
        marginHorizontal:15
    },
    portLogoWrap:{
        flexDirection:'row',
        marginHorizontal:20,
        marginVertical:20,
        justifyContent:'space-around'
    },
    portLogoContainer:{
        alignItems:'center',
        flexDirection:'column',
        marginRight:20
    },
    hubContainer:{
        marginVertical:20,
        backgroundColor:'#EFEFEF',
        borderRadius:15,
        width:329,
        marginHorizontal:15
    },
    hubLogoContainer:{
        alignItems:'center',
        flexDirection:'row',
        marginBottom:20,

    },
    hubLogoWrap:{
        flexDirection:'column',
        marginHorizontal:20,
        marginVertical:20,
        alignItems:'center'
    }
})