import React from 'react';
import { NavigationContainer, DrawerActions } from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {View,Button} from 'react-native';
import LoginScreen from './LoginScreen';
import AboutScreen from './About';
import SkillScreen from './SkillScreen';
import ProjectScreen from './ProjectScreen';
import AddScreen from './AddScreen';

const rootStack = createDrawerNavigator();

//Login
const LoginStack = createStackNavigator();
const LoginStackScreen = () =>(
    <LoginStack.Navigator>
        <LoginStack.Screen name="Login" component={LoginScreen}/>
    </LoginStack.Navigator>
)
//About
const AboutStack = createStackNavigator();
const AboutStackScreen = () => (
    <AboutStack.Navigator>
        <AboutStack.Screen name="About" component={AboutScreen} />
    </AboutStack.Navigator>
)
//Skill
const SkillStack = createStackNavigator();
const SkillStackScreen = () => (
    <SkillStack.Navigator>
        <SkillStack.Screen name="Skill" component={SkillScreen} />
    </SkillStack.Navigator>
)
//Project
const ProjectStack = createStackNavigator();
const ProjectStackScreen = () => (
    <ProjectStack.Navigator>
        <ProjectStack.Screen name="Project" component={ProjectScreen} />
    </ProjectStack.Navigator>
)
//Add
const AddStack = createStackNavigator();
const AddStackScreen = () => (
    <AddStack.Navigator>
        <AddStack.Screen name="Add" component={AddScreen} />
    </AddStack.Navigator>
)

//Tab
const Tab = createBottomTabNavigator();
const TabScreen =()=>(
    <Tab.Navigator>
        <Tab.Screen name="About" component={AboutStackScreen} />
        <Tab.Screen name="Skill" component={SkillStackScreen} />
        <Tab.Screen name="Project" component={ProjectStackScreen}/>
        <Tab.Screen name="Add" component={AddStackScreen}/>
</Tab.Navigator>
)
export default () =>(
    <NavigationContainer>
        <rootStack.Navigator>
            <rootStack.Screen name="Login" component={LoginStackScreen}/>
            <rootStack.Screen name="About" component={TabScreen}/>
        </rootStack.Navigator>
    </NavigationContainer>
)