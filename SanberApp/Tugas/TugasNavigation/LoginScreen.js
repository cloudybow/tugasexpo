import React, { Component } from 'react';
import {StyleSheet, 
        View, 
        Image,
        TouchableOpacity,
        Text,
        TextInput,
        StatusBar} from 'react-native';

export default class LoginScreen extends Component {
    constructor() {
        super();
        this.state = {
          value: 'Type Something',
        };
    }
    render(){
        return (
            <View style={styles.Container}>
                <StatusBar backgroundColor='blue'/>
                <View style={styles.imgContainer}>
                    <Image source={require('./images/logo.png')} style={{width:300,height:100}}/>
                </View>
                <View style={styles.bodyContainer}>
                    <Text style={{color:'#003366', fontSize:30, marginBottom:30}}>Login</Text>
                    <View style={styles.bodyBox}>
                        <Text style={{paddingBottom:10, color:'#003366'}}>Email :</Text>
                        <TextInput style={{ height: 40, borderColor: '#003366', borderWidth: 1, width:300}} 
                        onChangeText={(text) => this.setState({ value: text })} value={this.state.value}/>
                    </View>
                    <View style={styles.bodyBox}>
                        <Text style={{paddingBottom:10, color:'#003366'}}>Password :</Text>
                        <TextInput style={{ height: 40, borderColor: '#003366', borderWidth: 1, width:300}} 
                        onChangeText={(text) => this.setState({ value: text })} value={this.state.value}/>
                    </View>
                </View>
                <View style={styles.bottomContainer}>
                    <TouchableOpacity style={styles.button}>
                        <Text style={{color:'white',fontSize:20}}>Masuk</Text>
                    </TouchableOpacity>
                    <Text style={{color:'#003366', marginVertical:15, fontSize:15}}>Atau</Text>
                    <TouchableOpacity style={styles.button2}>
                        <Text style={{color:'white', fontSize:20}}>Daftar?</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    Container:{
        flex:1,
    },
    imgContainer:{
        paddingHorizontal:25,
        marginTop:30
    },
    bodyContainer:{
        marginTop:50,
        alignItems:'center',
    },
    bodyBox:{
        marginBottom:25
    },
    bottomContainer:{
        marginTop:30,
        flexDirection:'column',
        alignItems:'center'
    },
    button:{
        height:40, width:100,
        alignItems:'center',
        paddingVertical:5,
        backgroundColor:'#3EC6FF',
        borderRadius:30
    },
    button2:{
        backgroundColor:'#003366',
        height:40, width:100,
        alignItems:'center',
        paddingVertical:5,
        borderRadius:30
    }
})