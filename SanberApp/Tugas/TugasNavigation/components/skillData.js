import React, { Component } from 'react';
import {StyleSheet, 
        View, 
        Text,
        TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon2 from 'react-native-vector-icons/MaterialIcons';
//light-blue:  '#3EC6FF'
//dark-blue:  '#003366'

export default class SkillData extends Component {
    render(){
        let skill = this.props.skill;
        let icon = skill.iconName;
        return(
            <TouchableOpacity style={styles.Container}>
                <Icon name={icon} size={80}/>
                <View style={styles.text}>
                    <Text style={{fontSize:18,color:'#003366'}}>{skill.skillName}</Text>
                    <Text style={{fontSize:12,color:'#3EC6FF'}}>{skill.categoryName}</Text>
                    <Text style={{fontSize:25,color:'white',textAlign:'right'}}>{skill.percentageProgress}</Text>
                </View>
                <Icon2 name="navigate-next" size={80} style={styles.nextIco}/>
            </TouchableOpacity>
        );
    }
}

const styles=StyleSheet.create({
    Container:{
        marginTop:10,
        backgroundColor:'#B4E9FF',
        borderRadius:15,
        height:100,
        paddingVertical:5,
        paddingHorizontal:10,
        flexDirection:"row"
    },
    text:{
        marginVertical:10,
        marginHorizontal:20
    },
    nextIco:{
        position:'absolute',
        right:5,
        top:10
    }
})