import React, { Component } from 'react';
import {StyleSheet, 
        View, 
        Text,
        StatusBar,
        Image,
        TouchableOpacity,
        ScrollView,
        FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import data from './skillData.json';
import SkillData from './components/skillData'
//light-blue:  '#3EC6FF'
//dark-blue:  '#003366'

export default class skillScreen extends Component {
    render(){
        return(
            <View style={styles.Container}>
                <StatusBar barStyle='default'/>
                <Image source={require('./images/logo.png')} style={styles.headLogo}/>
                
                <View style={styles.headContainer}>
                    <Icon name="account-circle" size={45} color='#3EC6FF'/>
                    <View style={styles.headText}>
                        <Text style={{fontSize:12}}>Hai,</Text>
                        <Text style={{color:'#003366'}}>Afiyah Salsabila Arief</Text>
                    </View>
                </View>

                <View style={styles.skillContainer}>
                    <View style={styles.skillTitle}>
                        <Text style={{color:'#003366',fontSize:30}}>SKILL</Text>
                    </View>
                    
                    <View style={{height:50}}>
                    <ScrollView horizontal style={styles.skillButtonWrap}>
                        <TouchableOpacity style={styles.skillButton}>
                            <Text style={{color:'#003366'}}>Library/Framework</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.skillButton}>
                            <Text style={{color:'#003366'}}>Bahasa Pemrograman</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.skillButton}>
                            <Text style={{color:'#003366'}}>Teknologi</Text>
                        </TouchableOpacity>
                    </ScrollView>
                    </View>

                    <View style={styles.Flat}>
                    <FlatList 
                        data={data.items}
                        renderItem={(skill)=> <SkillData skill={skill.item}/>}
                        keyExtractor={(item)=>item.id}
                        ItemSeparatorComponent={()=><View style={{height:1}}/>}
                    />  
                    </View>
                </View>
            </View>
        );
    }
}

const styles=StyleSheet.create({
    container:{
        flex:1
    },
    headLogo:{
        width:200,
        height:65,
        marginLeft:155
    },
    headContainer:{
        flexDirection:'row',
        marginLeft:10,
        marginTop:20
    },
    headText:{
        flexDirection:'column',
        marginTop:5
    },
    skillContainer:{
        marginTop:15,
        marginHorizontal:20,
        height:1000
    },
    skillTitle:{
        borderBottomWidth:5,
        borderBottomColor:'#3EC6FF'
    },
    skillButtonWrap:{
        marginTop:10,
        flexDirection:'row',
    },
    skillButton:{
        backgroundColor:'#3EC6FF',
        alignItems:'center',
        height:35,
        paddingVertical:6.5,
        paddingHorizontal:10,
        borderRadius:10,
        marginRight:10
    },
    Flat:{
        height:480
    }
})